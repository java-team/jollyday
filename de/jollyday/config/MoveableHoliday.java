//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MoveableHoliday complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MoveableHoliday"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.example.org/Holiday}Holiday"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MovingCondition" type="{http://www.example.org/Holiday}MovingCondition" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoveableHoliday", propOrder = {
    "movingCondition"
})
@XmlSeeAlso({
    Fixed.class,
    ChristianHoliday.class
})
public abstract class MoveableHoliday
    extends Holiday
{

    @XmlElement(name = "MovingCondition")
    protected List<MovingCondition> movingCondition;

    /**
     * Gets the value of the movingCondition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the movingCondition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMovingCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MovingCondition }
     * 
     * 
     */
    public List<MovingCondition> getMovingCondition() {
        if (movingCondition == null) {
            movingCondition = new ArrayList<MovingCondition>();
        }
        return this.movingCondition;
    }

}
