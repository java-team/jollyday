//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für FixedWeekdayRelativeToFixed complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="FixedWeekdayRelativeToFixed"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.example.org/Holiday}Holiday"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="day" type="{http://www.example.org/Holiday}Fixed"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="which" type="{http://www.example.org/Holiday}Which" /&gt;
 *       &lt;attribute name="weekday" type="{http://www.example.org/Holiday}Weekday" /&gt;
 *       &lt;attribute name="when" type="{http://www.example.org/Holiday}When" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FixedWeekdayRelativeToFixed", propOrder = {
    "day"
})
public class FixedWeekdayRelativeToFixed
    extends Holiday
{

    @XmlElement(required = true)
    protected Fixed day;
    @XmlAttribute(name = "which")
    protected Which which;
    @XmlAttribute(name = "weekday")
    protected Weekday weekday;
    @XmlAttribute(name = "when")
    protected When when;

    /**
     * Ruft den Wert der day-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Fixed }
     *     
     */
    public Fixed getDay() {
        return day;
    }

    /**
     * Legt den Wert der day-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Fixed }
     *     
     */
    public void setDay(Fixed value) {
        this.day = value;
    }

    /**
     * Ruft den Wert der which-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Which }
     *     
     */
    public Which getWhich() {
        return which;
    }

    /**
     * Legt den Wert der which-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Which }
     *     
     */
    public void setWhich(Which value) {
        this.which = value;
    }

    /**
     * Ruft den Wert der weekday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Weekday }
     *     
     */
    public Weekday getWeekday() {
        return weekday;
    }

    /**
     * Legt den Wert der weekday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Weekday }
     *     
     */
    public void setWeekday(Weekday value) {
        this.weekday = value;
    }

    /**
     * Ruft den Wert der when-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link When }
     *     
     */
    public When getWhen() {
        return when;
    }

    /**
     * Legt den Wert der when-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link When }
     *     
     */
    public void setWhen(When value) {
        this.when = value;
    }

}
