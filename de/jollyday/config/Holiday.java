//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Holiday complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Holiday"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="validFrom" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="validTo" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="every" type="{http://www.example.org/Holiday}HolidayCycleType" default="EVERY_YEAR" /&gt;
 *       &lt;attribute name="descriptionPropertiesKey" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="localizedType" type="{http://www.example.org/Holiday}HolidayType" default="OFFICIAL_HOLIDAY" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Holiday")
@XmlSeeAlso({
    RelativeToFixed.class,
    FixedWeekdayInMonth.class,
    RelativeToWeekdayInMonth.class,
    IslamicHoliday.class,
    FixedWeekdayBetweenFixed.class,
    HinduHoliday.class,
    MoveableHoliday.class,
    HebrewHoliday.class,
    EthiopianOrthodoxHoliday.class,
    FixedWeekdayRelativeToFixed.class,
    RelativeToEasterSunday.class
})
public abstract class Holiday {

    @XmlAttribute(name = "validFrom")
    protected Integer validFrom;
    @XmlAttribute(name = "validTo")
    protected Integer validTo;
    @XmlAttribute(name = "every")
    protected String every;
    @XmlAttribute(name = "descriptionPropertiesKey")
    protected String descriptionPropertiesKey;
    @XmlAttribute(name = "localizedType")
    protected HolidayType localizedType;

    /**
     * Ruft den Wert der validFrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getValidFrom() {
        return validFrom;
    }

    /**
     * Legt den Wert der validFrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setValidFrom(Integer value) {
        this.validFrom = value;
    }

    /**
     * Ruft den Wert der validTo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getValidTo() {
        return validTo;
    }

    /**
     * Legt den Wert der validTo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setValidTo(Integer value) {
        this.validTo = value;
    }

    /**
     * Ruft den Wert der every-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEvery() {
        if (every == null) {
            return "EVERY_YEAR";
        } else {
            return every;
        }
    }

    /**
     * Legt den Wert der every-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEvery(String value) {
        this.every = value;
    }

    /**
     * Ruft den Wert der descriptionPropertiesKey-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescriptionPropertiesKey() {
        return descriptionPropertiesKey;
    }

    /**
     * Legt den Wert der descriptionPropertiesKey-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescriptionPropertiesKey(String value) {
        this.descriptionPropertiesKey = value;
    }

    /**
     * Ruft den Wert der localizedType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link HolidayType }
     *     
     */
    public HolidayType getLocalizedType() {
        if (localizedType == null) {
            return HolidayType.OFFICIAL_HOLIDAY;
        } else {
            return localizedType;
        }
    }

    /**
     * Legt den Wert der localizedType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link HolidayType }
     *     
     */
    public void setLocalizedType(HolidayType value) {
        this.localizedType = value;
    }

}
