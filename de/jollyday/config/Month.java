//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 generiert 
// Siehe <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2019.12.18 um 09:41:15 PM CET 
//


package de.jollyday.config;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für Month.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Month"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="JANUARY"/&gt;
 *     &lt;enumeration value="FEBRUARY"/&gt;
 *     &lt;enumeration value="MARCH"/&gt;
 *     &lt;enumeration value="APRIL"/&gt;
 *     &lt;enumeration value="MAY"/&gt;
 *     &lt;enumeration value="JUNE"/&gt;
 *     &lt;enumeration value="JULY"/&gt;
 *     &lt;enumeration value="AUGUST"/&gt;
 *     &lt;enumeration value="SEPTEMBER"/&gt;
 *     &lt;enumeration value="OCTOBER"/&gt;
 *     &lt;enumeration value="NOVEMBER"/&gt;
 *     &lt;enumeration value="DECEMBER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Month")
@XmlEnum
public enum Month {

    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER;

    public String value() {
        return name();
    }

    public static Month fromValue(String v) {
        return valueOf(v);
    }

}
